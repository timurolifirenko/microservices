﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebHooksSample.WebHooks
{
    public class WebhookEvent
    {
        /// <summary>
        /// Webhook unique name
        /// </summary>
        [Required]
        public string WebhookName { get; set; }

        /// <summary>
        /// Webhook data as JSON string.
        /// </summary>
        public string Data { get; set; }
    }
}
