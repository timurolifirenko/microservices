﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebHooksSample.Models;

namespace WebHooksSample.WebHooks
{
    public class WebhookSubscriptionManager
    {
        public const string WebhookSubscriptionSecretPrefix = "whs_";
        IRepository<WebhookSubscription> _webhookSubscriptionRepository;
	

	public async Task AddSubscriptionAsync(WebhookSubscription webhookSubscription)
        {
            webhookSubscription.Secret = WebhookSubscriptionSecretPrefix + Guid.NewGuid().ToString("N");
            await _webhookSubscriptionRepository.InsertAsync(webhookSubscription);
        }
    }
}
