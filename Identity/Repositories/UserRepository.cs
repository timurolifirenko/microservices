﻿using Identity.Interfaces;
using Identity.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> _col;

        public UserRepository(IMongoDatabase db)
        {
            _col = db.GetCollection<User>(User.DocumentName);
        }

        public User GetUser(string email) =>
            _col.Find(u => u.Email == email).FirstOrDefault();

        public void InsertUser(User user) =>
            _col.InsertOne(user);
    }
}
