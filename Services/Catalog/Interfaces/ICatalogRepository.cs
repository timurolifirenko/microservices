﻿using Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Interfaces
{
    public interface ICatalogRepository
    {
        List<CatalogItem> GetCatalogItems();
        CatalogItem GetCatalogItem(Guid catalogItemId);
        void InsertCatalogItem(CatalogItem catalogItem);
        void UpdateCatalogItem(CatalogItem catalogItem);
        void DeleteCatalogItem(Guid catalogItemId);
    }
}
